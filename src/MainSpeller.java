import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Polygon;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;


public class MainSpeller {

	protected Shell shell;

    private JFrame mainMap;
    private Polygon[] poly;
	/**
	 * Launch the application.
	 * @param args
	 */
    
    public MainSpeller() {

        initComponents();

    }
    
	public static void main(String[] args) {
		try {
			MainSpeller window = new MainSpeller();
			window.open();
			   SwingUtilities.invokeLater(new Runnable() {
		            @Override
		            public void run() {
		                new MainSpeller();
		            }
		        });

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Open the window.
	 */
	public void open() {
		Display display = Display.getDefault();
		createContents();
		shell.open();
		shell.layout();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

	/**
	 * Create contents of the window.
	 */
	protected void createContents() {
		shell = new Shell();
		shell.setSize(450, 300);
		shell.setText("Hex-O-Spell");

	}

	 private void initComponents() {

	        mainMap = new JFrame();
	        mainMap.setResizable(false);

	        mainMap.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	       
	        int x = 250;
			int y = 200;
			int r = 100;		
			poly = new Polygon[6];
			
			for(int numOfHexs = 0;numOfHexs <6 ; numOfHexs++ )
			{
				x = x + (int) (174*Math.sin(numOfHexs*2*Math.PI/6));
				y = y + (int) (174*Math.cos(numOfHexs*2*Math.PI/6));
				poly[numOfHexs] = new Polygon();
				for(int HexSide = 0; HexSide < 6; HexSide++) {
					int xPoint = (int) (r*Math.cos(HexSide*2*Math.PI/6));
					int yPoint = (int) (r*Math.sin(HexSide*2*Math.PI/6));
					
					poly[numOfHexs].addPoint(x + xPoint, y + yPoint);
				}
			}
		        JPanel p = new JPanel() {
		            @Override
		            protected void paintComponent(Graphics g) {
		                super.paintComponent(g);
		                g.setColor(Color.BLUE);
		                for(int x = 0 ; x < 6 ; x++)
		                {
		                	g.drawPolygon(poly[x]);
		                }
		            }
		        
		            @Override
		            public Dimension getPreferredSize() {
		                return new Dimension(800, 600);
		            }
		        
		        };
			
	        mainMap.add(p);
	        mainMap.pack();
	        mainMap.setVisible(true);		
	    }
	    
	
}
